let userScore = 0;
let computerScore = 0;

const updateUserScore = document.getElementById("userScore");
const updateComputerScore = document.getElementById("computerScore");

const rock = document.getElementById("rock");
const paper = document.getElementById("paper");
const scissors = document.getElementById("scissors");

const restart = document.getElementById("btn");

    // Restart page

restart.addEventListener('click', restartPage);
function restartPage(){
    window.location.reload();
}

function computerChoice(){
    const choices = ["rock", "paper", "scissors"];
    randomNumber=Math.floor(Math.random() * 3);
    return choices[randomNumber];
}
console.log(computerChoice());


rock.addEventListener('click', function(){  // User choose rock


    if(computerChoice() == "rock"){
        document.getElementById("p").innerHTML = "<b>Withdraw</b>";
        updateComputerScore.innerHTML = "Computer:" + computerScore;
        updateUserScore.innerHTML = "User:" + userScore;

        document.getElementById("rock").classList.add("yellow-border");
        setTimeout(function(){document.getElementById("rock").classList.remove("yellow-border")},1000)
    }

    else if(computerChoice() == "paper"){
        document.getElementById("p").innerHTML = "<b>Computer choose paper, Computer won!</b>";
        computerScore++;
        updateComputerScore.innerHTML = "Computer:" + computerScore;
        updateUserScore.innerHTML = "User:" + userScore;

        document.getElementById("rock").classList.add("red-border");
        setTimeout(function(){document.getElementById("rock").classList.remove("red-border")},1000)
    }
    else if(computerChoice() == "scissors"){
        document.getElementById("p").innerHTML = "<b>Computer choose scissors, You won!</b>";
        userScore++;
        updateComputerScore.innerHTML = "Computer:" + computerScore;
        updateUserScore.innerHTML = "User:" + userScore;

        document.getElementById("rock").classList.add("green-border");
        setTimeout(function(){document.getElementById("rock").classList.remove("green-border")},1000)
    }

})

paper.addEventListener('click', function(){ // User choose paper

    

    if(computerChoice() == "rock"){
        document.getElementById("p").innerHTML = "<b>Computer choose rock, You won!</b>";
        userScore++;
        updateComputerScore.innerHTML = "Computer:" + computerScore;
        updateUserScore.innerHTML = "User:" + userScore;

        document.getElementById("paper").classList.add("green-border");
        setTimeout(function(){document.getElementById("paper").classList.remove("green-border")},1000)
    }

    else if(computerChoice() == "paper"){
        document.getElementById("p").innerHTML = "<b>Withdraw</b>";
        updateComputerScore.innerHTML = "Computer:" + computerScore;
        updateUserScore.innerHTML = "User:" + userScore;

        document.getElementById("paper").classList.add("yellow-border");
        setTimeout(function(){document.getElementById("paper").classList.remove("yellow-border")},1000)
    }

    else if(computerChoice() == "scissors"){
        document.getElementById("p").innerHTML = "<b>Computer choose scissors, Computer won!</b>";
        computerScore++;
        updateComputerScore.innerHTML = "Computer:" + computerScore;
        updateUserScore.innerHTML = "User:" + userScore;

        document.getElementById("paper").classList.add("red-border");
        setTimeout(function(){document.getElementById("paper").classList.remove("red-border")},1000)
    }
    
})

scissors.addEventListener('click', function(){  // User choose scissors

    if(computerChoice() == "rock"){
        document.getElementById("p").innerHTML = "<b>Computer choose rock, Computer won!</b>";
        computerScore++;
        updateComputerScore.innerHTML = "Computer:" + computerScore;
        updateUserScore.innerHTML = "User:" + userScore;

        document.getElementById("scissors").classList.add("red-border");
        setTimeout(function(){document.getElementById("scissors").classList.remove("red-border")},1000)
    }

    else if(computerChoice() == "paper"){
        document.getElementById("p").innerHTML = "<b>Computer choose paper, You won!</b>";
        userScore++;
        updateComputerScore.innerHTML = "Computer:" + computerScore;
        updateUserScore.innerHTML = "User:" + userScore;

        document.getElementById("scissors").classList.add("green-border");
        setTimeout(function(){document.getElementById("scissors").classList.remove("green-border")},1000)
    }

    else if(computerChoice() == "scissors"){
        document.getElementById("p").innerHTML = "<b>Withdraw</b>";
        updateComputerScore.innerHTML = "Computer:" + computerScore;
        updateUserScore.innerHTML = "User:" + userScore;

        document.getElementById("scissors").classList.add("yellow-border");
        setTimeout(function(){document.getElementById("scissors").classList.remove("yellow-border")},1000)
    }
})

